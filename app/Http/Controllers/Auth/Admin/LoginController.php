<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

use App\Models\User;
use App\Notifications\VerifyRegistration;

class LoginController extends Controller
{
 

    use AuthenticatesUsers;

    protected $redirectTo = '/admin';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.admin.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|string',
        ]);

            if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember )) {
                return redirect()->intended(route('admin.index'));
            } else {
                session()->flush('sticky_error', 'Invalid Information');
                return redirect()->route('admin.login');
            }
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect()->route('admin.login');
    }
    
}
