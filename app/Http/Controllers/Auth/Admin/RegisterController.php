<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

use App\Models\District;
use App\Models\Division;

use App\Notifications\VerifyRegistration;

class RegisterController extends Controller
{

    use RegistersUsers;

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        $divisions = Division::orderBy('priority', 'asc')->get();
        $districts = District::orderBy('name', 'asc')->get();

        return view('auth.register', compact('divisions', 'districts'));
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name'       => 'required|string|max:255',
            'last_name'        => 'required|string|max:255',
            'phone_no'         => 'required|max:15',
            'email'            => 'required|string|email|max:255|unique:users',
            'password'         => 'required|string|min:6|confirmed',
            'street_address'   => 'required|max:100',
            'division_id'      => 'required|numeric',
            'district_id'      => 'required|numeric',
        ]);
    }

    protected function register(Request $request)
    {
        $user = User::create([
            'first_name'     => $request->first_name,
            'last_name'      => $request->last_name,
            'username'       => str_slug($request->first_name.$request->last_name),  
            'phone_no'       => $request->phone_no,
            'email'          => $request->email,
            'password'       => Hash::make($request->password),
            'street_address' => $request->street_address,
            'division_id'    => $request->division_id,
            'district_id'    => $request->district_id,
            'ip_address'     => request()->ip(),
            'remember_token' => str_random(50),
            'status'         => 0,
        ]);

        $user->notify(new VerifyRegistration($user));

        session()->flash('success', 'A confirmation email has sent to you.. Please Check and verify your email');

        return redirect('/');
    }
}
