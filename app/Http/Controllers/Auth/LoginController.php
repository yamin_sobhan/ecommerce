<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

use App\Models\User;
use App\Notifications\VerifyRegistration;

class LoginController extends Controller
{
 

    use AuthenticatesUsers;

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|string',
        ]);

        $user = User::where('email', $request->email)->first();
        if ($user->status == 1) {

            if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember )) {
                return redirect()->intended(route('index'));
            } else {
                session()->flush('errors', 'Please login first');
                return redirect()->route('login');
            }
        }else {
            if (!is_null($user)) {
                $user->notify(new VerifyRegistration($user));

                session()->flush('success', 'A new confirmation mail has sent to you...');
                return redirect('/');
            }else {
                session()->flush('errors', 'Please login first');
                return redirect()->route('login');
            }
        }
    }
}
