<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Brand;
use Image;

class BrandsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    
    public function index()
    {
        $brands = Brand::orderBy('id', 'desc')->get();
        return view('backend.pages.brands.index', compact('brands'));
    }

    public function create()
    {
        return view('backend.pages.brands.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'image' => 'nullable|image',
        ]);

        $brand = new Brand;

        $brand->name = $request->name;
        $brand->description = $request->description;
        if ($request->image) {
            $image = $request->file('image');
              $img = time() . '.'. $image->getClientOriginalExtension();
              $location = public_path('images/brands/' .$img);
              Image::make($image)->save($location);
              $brand->image = $img;
        }

        $brand->save();

        return redirect()->route('admin.brand.create');
    }

    public function edit($id)
    {
        $brand = Brand::find($id);
        return view('backend.pages.brands.edit', compact('brand'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name'  => 'required',
            'image' => 'nullable|image',
        ]);

        $brand = Brand::find($id);

        $brand->name = $request->name;
        $brand->description = $request->description;

        $brand->save();

        return redirect()->route('admin.brands');
    }

    public function delete($id)
    {
        $brand = Brand::find($id);

        $brand->delete();

        return back();
    }
    
}
