<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Category;
use Image;

class CategoriesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    
    public function index()
    {
        $categories = Category::orderBy('id', 'desc')->get();
        return view('backend.pages.categories.index', compact('categories'));
    }

    public function create()
    {
        $main_categories = Category::orderBy('name', 'desc')->where('parent_id', NULL)->get();
        return view('backend.pages.categories.create', compact('main_categories'));
    }

    public function edit($id)
    {   $main_categories = Category::orderBy('name', 'desc')->where('parent_id', NULL)->get();
        $category = Category::find($id);
        return view('backend.pages.categories.edit')->with('category', $category)->with('main_categories', $main_categories);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'image' => 'nullable|image',
        ]);

        $category = new Category;

        $category->name = $request->name;
        $category->description = $request->description;
        $category->parent_id = $request->parent_id;
        
        if ($request->image) {
              $image = $request->file('image');
              $img = time() . '.'. $image->getClientOriginalExtension();
              $location = public_path('images/categories/' .$img);
              Image::make($image)->save($location);
              $category->image = $img;
          }

        $category->save();

        return redirect()->route('admin.category.create');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name'  => 'required',
            'image' => 'nullable|image',
        ]);

        $category = Category::find($id);

        $category->name = $request->name;
        $category->description = $request->description;
        $category->parent_id = $request->parent_id;

        $category->save();

        return redirect()->route('admin.categories');
    }

    public function delete($id)
    {
        $category = Category::find($id);

        if (!is_null($category)) {
            if ($category->parent_id == NULL) {
                $sub_categories = Category::orderBy('name', 'desc')->where('parent_id', $category->id)->get();

                foreach ($sub_categories as $sub) {
                    $sub->delete();
                }
            }


            $category->delete();
        }

        session()->flush('success', 'Catagory Deleted Successfully');

        return back();
    }
}
