<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Division;
use App\Models\District;

class DistrictsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    
    public function index()
    {
        $districts = District::orderBy('name', 'asc')->get();
        return view('backend.pages.districts.index', compact('districts'));
    }

    public function create()
    {
        $divisions = Division::orderBy('priority', 'asc')->get();
        return view('backend.pages.districts.create', compact('divisions'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'division_id' => 'required',
        ]);

        $district = new District;

        $district->name = $request->name;
        $district->division_id = $request->division_id;
        
        $district->save();

        return redirect()->route('admin.district.create');
    }

    public function edit($id)
    {
        $divisions = Division::orderBy('priority', 'asc')->get();
        $district = District::find($id);
        return view('backend.pages.districts.edit', compact('district', 'divisions'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name'  => 'required',
            'division_id' => 'required',
        ]);

        
        $district = District::find($id);

        $district->name = $request->name;
        $district->division_id = $request->division_id;
        
        $district->save();


        return redirect()->route('admin.districts');
    }

    public function delete($id)
    {
        $district = District::find($id);

        $district->delete();

        return back();
    }
    
}
