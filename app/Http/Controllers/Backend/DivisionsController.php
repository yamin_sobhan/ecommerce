<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Division;
use App\Models\District;

class DivisionsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    
    public function index()
    {
        $divisions = Division::orderBy('priority', 'asc')->get();
        return view('backend.pages.divisions.index', compact('divisions'));
    }

    public function create()
    {
        return view('backend.pages.divisions.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'priority' => 'required',
        ]);

        $division = new Division;

        $division->name = $request->name;
        $division->priority = $request->priority;
        
        $division->save();

        return redirect()->route('admin.division.create');
    }

    public function edit($id)
    {
        $division = Division::find($id);
        return view('backend.pages.divisions.edit', compact('division'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name'  => 'required',
            'priority' => 'required',
        ]);

        
        $division = Division::find($id);

        $division->name = $request->name;
        $division->priority = $request->priority;
        
        $division->save();


        return redirect()->route('admin.divisions');
    }

    public function delete($id)
    {
        $division = Division::find($id);

        if (!Is_null($division)) {

            $districts = District::where('division_id', $division->id)->get();

            foreach ($districts as $district) {
                $district->delete();
            }

            $division->delete();
        }

        return back();
    }
    
}
