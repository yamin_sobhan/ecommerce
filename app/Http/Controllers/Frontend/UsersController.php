<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Models\User;
use App\Models\Division;
use App\Models\District;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        $user = Auth::user();
        return view('frontend.pages.users.dashboard', compact('user'));
    }

    public function profile()
    {

        $divisions = Division::orderBy('priority', 'asc')->get();
        $districts = District::orderBy('name', 'asc')->get();

        $user = Auth::user();
        return view('frontend.pages.users.profile', compact('user', 'divisions', 'districts'));
    }

    public function profileUpdate(Request $request)
    {

        $user = Auth::user();


        $this->validate($request, [
            'first_name'       => 'required|string|max:255',
            'last_name'        => 'required|string|max:255',
            'phone_no'         => 'required|max:15|unique:users,phone_no,'.$user->id,
            'username'         => 'required|string|alpha_dash|max:100|unique:users,username,'.$user->id,
            'email'            => 'required|string|email|max:255|unique:users,email,'.$user->id,
            'street_address'   => 'required|max:100',
            'division_id'      => 'required|numeric',
            'district_id'      => 'required|numeric',
        ]);

        
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->phone_no = $request->phone_no;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->street_address = $request->street_address;
        $user->division_id = $request->division_id;
        $user->district_id = $request->district_id;
        $user->shipping_address = $request->shipping_address;

        if ($request->password != NULL || $request->password != "") {
            $user->password = Hash::make($request->password);
        }

        $user->ip_address = request()->ip();

        $user->save();

        session()->flash('success', 'User Profile updated !!');
        return back();

    }
}
