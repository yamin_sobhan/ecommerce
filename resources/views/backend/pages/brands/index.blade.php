@extends('backend.layouts.master')

@section('content')
  <div class="main-panel">
    <div class="content-wrapper">

      <div class="card">
        <div class="card-header">
          Manage Brand
        </div>
        <div class="card-body">
            <table class="table table-hover table-striped" id="dataTable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Brand Name</th>
                    <th>Brand Description</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($brands as $brand)
                    <tr>
                        <td>{{ $brand->id }}</td>
                        <td>{{ $brand->name }}</td>
                        <td>{{ $brand->description }}</td>
                        <td>
                          <a href="{{ route('admin.brand.edit', $brand->id) }}" class="btn btn-success">Edit</a>
                          <a href="#deleteModal{{ $brand->id }}" data-toggle="modal" class="btn btn-danger">Delete</a>

                          <div class="modal fade" tabindex="-1" role="dialog" id="deleteModal{{ $brand->id }}">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title">Are you sure to delete?</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  <form action="{{ route('admin.brand.delete', $brand->id) }}" method="post">
                                    {{ csrf_field() }}

                                    <button type="submit" class="btn btn-danger">Delete</button> 
                                  </form>
                                  
                                  
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-primary">Cancel</button>
                                  
                                </div>
                              </div>
                            </div>
                          </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>#</th>
                    <th>Brand Name</th>
                    <th>Brand Description</th>
                    <th>Action</th>
                </tr>
                </tfoot>
            </table>
        </div>
      </div>

    </div>
  </div>
  <!-- main-panel ends -->
@endsection
