
<div class="list-group">
  @foreach (App\Models\Category::orderBy('name', 'asc')->where('parent_id', NULL)->get() as $parent)

    <a href="#main-{{ $parent->id }}" class="list-group-item list-group-item-action" data-toggle="collapse">
        {{ $parent->name }}
    </a>
    
  <div class="child-rows collapse" id="main-{{ $parent->id }}">
  @foreach (App\Models\Category::orderBy('name', 'asc')->where('parent_id', $parent->id)->get() as $child)
    <a href="{{ route('categories.show', $child->id) }}" class="list-group-item list-group-item-action">
        {{$child->name}}
    </a>
  @endforeach
  </div>  

  @endforeach
</div>
